/**
 * Gulp Packages
 */

// General
const { task, series, parallel, src, dest, watch } = require('gulp')
const del = require('del')
const wiredep = require('wiredep').stream
const gulpif = require('gulp-if')
const useref = require('gulp-useref')
const plumber = require('gulp-plumber')
const flatten = require('gulp-flatten')
const rename = require('gulp-rename')
const imagemin = require('gulp-imagemin')
const browserSync = require('browser-sync').create()

// Scripts
const uglify = require('gulp-uglify')

// Styles
const sass = require('gulp-dart-sass')
const prefix = require('gulp-autoprefixer')
const minify = require('gulp-cssnano')
const cssimport = require('gulp-cssimport')

// Pug
const pug = require('gulp-pug-i18n')
const marked = require('marked')
const glob = require('glob')

// Build tasks to run and watch
const buildTypes = ['scripts', 'styles', 'pug', 'static', 'fonts', 'root']

// Paths to project folders
const paths = {
  input: 'src/**',
  output: 'dist/',
  scripts: {
    watch: 'src/js/**',
    input: 'dist/js/**',
    output: 'dist/js/'
  },
  styles: {
    watch: 'src/sass/**',
    input: 'src/sass/*.{scss,sass}',
    output: 'dist/css/',
    wiredep: 'src/sass'
  },
  pug: {
    watch: ['src/pages/**', 'src/locale/*.yml'],
    input: 'src/pages/*.pug',
    output: 'dist/',
    locales: 'src/locale/*.yml',
    filename: '{{lang}}{-{{region}}}/{{basename}}.html',
    wiredep: 'src/pages'
  },
  static: {
    input: 'src/static/**',
    output: 'dist/static/'
  },
  fonts: {
    input: 'bower_components/font-awesome/fonts/*',
    output: 'dist/static/fonts/'
  },
  root: {
    input: ['src/root/*', 'src/root/.*'],
    output: 'dist/'
  }
}

/**
 * tasks
 */

// Minify, and concatenate scripts
task('build:scripts', () =>
  src(paths.scripts.input)
    .pipe(plumber())
    .pipe(uglify())
    .pipe(dest(paths.scripts.output))
    .pipe(browserSync.stream())
)

// Process and minify Sass files
task('build:styles', () =>
  src(paths.styles.input)
    .pipe(plumber())
    .pipe(
      sass({
        outputStyle: 'expanded',
        sourceComments: true
      })
    )
    .pipe(flatten())
    .pipe(
      prefix({
        cascade: true,
        remove: true
      })
    )
    .pipe(cssimport({}))
    .pipe(dest(paths.styles.output))
    .pipe(rename({ suffix: '.min' }))
    .pipe(
      minify({
        discardComments: {
          removeAll: true
        }
      })
    )
    .pipe(dest(paths.styles.output))
    .pipe(browserSync.stream())
)

// Compile Pug files into output folder
task('build:pug', () =>
  src(paths.pug.input)
    .pipe(plumber())
    // Combine JS tags
    .pipe(
      useref({
        searchPath: ['.', 'src']
      })
    )
    .pipe(
      gulpif(
        '**/*.pug',
        pug({
          i18n: {
            locales: paths.pug.locales,
            filename: paths.pug.filename
          },
          data: {
            // Markdown
            // Add &nbsp; before punctuation
            md: t => marked(t).replace(/ (\!|\?|:)/, '&nbsp;$1'),
            glob
          }
        })
      )
    )
    .pipe(dest(paths.pug.output))
    .pipe(browserSync.stream())
)

// Inject bower components
task('wiredep', () => {
  // Styles
  src(paths.styles.wiredep + '/**')
    .pipe(wiredep())
    .pipe(dest(paths.styles.wiredep))

  // Pages
  src(paths.pug.wiredep + '/**')
    .pipe(
      wiredep({
        ignorePath: /^(\.\.\/)*\.\./
      })
    )
    .pipe(dest(paths.pug.wiredep))
})

// Copy static files into output folder
task('build:static', () =>
  src(paths.static.input)
    .pipe(plumber())
    // Optimize images
    .pipe(imagemin())
    .pipe(dest(paths.static.output))
    .pipe(browserSync.stream())
)

// Copy font files into output folder
task('build:fonts', () =>
  src(paths.fonts.input)
    .pipe(plumber())
    .pipe(dest(paths.fonts.output))
    .pipe(browserSync.stream())
)

// Copy root files into output folder
task('build:root', () =>
  src(paths.root.input)
    .pipe(plumber())
    .pipe(dest(paths.root.output))
    .pipe(browserSync.stream())
)

// Remove pre-existing content from output folder
task('clean:dist', () => {
  const delDist = () => del(paths.output)
  let tries = 5
  // Try to delete folder multiple times
  return delDist().catch(e => {
    if (tries != 0) {
      tries--
      return new Promise(resolve => setTimeout(resolve, 500)).then(delDist)
    }
    throw e
  })
})

/**
 * Task Runners
 */

// Build project
task('build', parallel(...buildTypes.map(t => `build:${t}`)))

// Clean and Build project
task('default', series('clean:dist', 'build'))

// Watch files and serve files with Browsersync
task(
  'start',
  series('default', () => {
    buildTypes.forEach(type => {
      const path = paths[type].watch || paths[type].input
      watch(path, series(`build:${type}`))
    })
    //watch('bower.json', ['wiredep'])
    browserSync.init({
      server: paths.output
    })
  })
)
