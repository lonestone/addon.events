const glob = require("glob");
const sharp = require("sharp");

const settings = {
  landscapes: {
    width: 1500,
    height: 1000,
    thumbWidth: 300,
    thumbHeight: 200
  },
  portraits: {
    width: 1000,
    height: 1500,
    thumbWidth: 200,
    thumbHeight: 300,
    rotate: -90
  }
};

function padLeft(n, length) {
  if (typeof n === "number") n = n.toString();
  return n.length < length ? padLeft(`0${n}`, length) : n;
}

// Number of current photo
let i = 0;

glob("./landscapes/*.@(jpg|JPG)", (error, files) => {
  if (error) throw error;
  resizeImages(files, settings.landscapes);
});

glob("./portraits/*.@(jpg|JPG)", (error, files) => {
  if (error) throw error;
  resizeImages(files, settings.portraits);
});

function resizeImages(
  files,
  { width, height, thumbWidth, thumbHeight, rotate = 0 }
) {
  files.forEach(file => {
    i++;
    const outputPath = `./photos/${padLeft(i, 3)}`;
    const sharpImage = sharp(file).rotate(rotate);
    sharpImage.resize(width, height).toFile(`${outputPath}.jpg`);
    sharpImage
      .resize(thumbWidth, thumbHeight)
      .toFile(`${outputPath}_thumb.jpg`);
  });
}
