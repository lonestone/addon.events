# ADDON Website

This is the source of [https://addon.events]()

This project uses:

- `gulp` to build the static website
- `bower` to manage the web packages
- `pug` for the templates
- `sass` for CSS preprocessing

## Dev

### Install

Install yarn and bower dependencies:

    yarn install

Run Gulp with server and livereload:

    yarn start

Go to [http://localhost:3000/fr-FR/](http://localhost:3000/fr-FR/)

### Add a Bower package

Install package (eg: jquery):

    yarn run bower-install --save jquery

Update bower script tags in pug files:

    yarn run wiredep

Script tags of bower packages will be inserted between `<!-- bower:js -->` and `<!-- endbower -->` comment tags.

### Resize photos

To resize photos:

- Add landscape photos to `scripts/resize/landscapes`
- Add portrait photos to `scripts/resize/portraits`
- Check settings in `scripts/resize/index.js`
- Execute:

  cd scripts/resize
  npm install
  npm start

## Prod

Install dependencies and build static website:

    yarn install && yarn run build

Put `./dist` folder behind an HTTP server (Nginx, Apache2...).
