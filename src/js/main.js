$(document).ready(function () {
  // Sticky Nav
  $('#main-nav').sticky({
    topSpacing: 0,
  })

  // Tooltips (reassurements)
  $('[data-toggle="tooltip"]').tooltip()

  // Days Counter
  var $dayscounter = $('.reassurement-dayscounter span')
  if ($dayscounter.length !== 0) {
    var startDate = new Date('2021-05-25T02:00:00.0000Z')
    $dayscounter.text(
      $dayscounter
        .text()
        .replace(
          '{XXX}',
          Math.ceil((startDate.getTime() - new Date().getTime()) / 86400000)
        )
    )
  }

  // Photos
  $('.gallery').magnificPopup({
    delegate: 'a',
    type: 'image',
    gallery: {
      enabled: true,
    },
    mainClass: 'mfp-fade',
  })

  // Map
  if (typeof mapboxgl === 'undefined' || $('#map').length === 0) return

  var position = [-1.6739777, 48.1050044]
  mapboxgl.accessToken =
    'pk.eyJ1IjoibG9uZXN0b25lIiwiYSI6ImNpcjNvdWltbTAwMjdpNG1jOGo5aWhjaHkifQ.M1PHd0GZhSFe7s7jJNlF5g'
  var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/lonestone/cj2hlkdxb00182rnontfjoiv7',
    interactive: false,
    center: position,
    zoom: 15.5,
    attributionControl: false,
  })
  var nav = new mapboxgl.NavigationControl()
  map.addControl(nav, 'top-left')

  // Event places
  var popups = [
    {
      id: 'mda',
      position: [-1.6781727, 48.1049632],
    },
  ]
  var $marker = $('.map-marker').remove()
  for (var i = 0; i < popups.length; i++) {
    var $popup = $('.map-popup-' + popups[i].id).remove()
    var popup = new mapboxgl.Popup({
      closeButton: false,
      closeOnClick: false,
      offset: [0, -40],
    }).setDOMContent($popup[0])
    new mapboxgl.Marker($marker.clone()[0], { offset: [-18, -36] })
      .setLngLat(popups[i].position)
      .setPopup(popup)
      .addTo(map)
      .togglePopup()
  }

  // Train station
  new mapboxgl.Marker($('.map-marker-train').remove()[0], {
    offset: [-18, -36],
  })
    .setLngLat([-1.6723063, 48.1035115])
    .addTo(map)
    .togglePopup()
})
